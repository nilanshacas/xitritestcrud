import { Component, Input, OnInit } from '@angular/core';
import { observable } from 'rxjs';
import { PostApiService } from 'src/app/post-api.service';


@Component({
  selector: 'app-add-edit-post',
  templateUrl: './add-edit-post.component.html',
  styleUrls: ['./add-edit-post.component.css']
})
export class AddEditPostComponent implements OnInit {



  constructor(private service:PostApiService) { }
  
  @Input() disableControls:any;

  isDisable: boolean = false;

  @Input() post:any;
  id: number = 0;
  title: string = "";
  body: string = "";
  status: string = "";
  active: boolean = false;


  ngOnInit(): void {
    this.id = this.post.id;
    this.title = this.post.title;
    this.body = this.post.body;
    this.status = this.post.status;
    this.active = this.post.active;

    this.isDisable = this.disableControls;
  }

  addPost(){
    var post = {
      title:this.title,
      body:this.body,
    };

    this.service.addPost(post).subscribe(res =>{
      var closeModalBtn = document.getElementById('add-edit-modal-close')
      if(closeModalBtn){
        closeModalBtn.click()
      }

      var showAddSuccess = document.getElementById('add-success-alert')
      if(showAddSuccess){
        showAddSuccess.style.display = "block";
      }

      setTimeout(function(){
        if(showAddSuccess){
          showAddSuccess.style.display = "none"
        }
      }, 4000);
    });
  }

  updatePost(){
    var post = {
      id: this.id,
      title:this.title,
      body:this.body,
    };
    var id:number = this.id;
    this.service.updatePost(id,post).subscribe(res =>{
      var closeModalBtn = document.getElementById('add-edit-modal-close')
      if(closeModalBtn){
        closeModalBtn.click()
      }

      var showUpdateSuccess = document.getElementById('update-success-alert')
      if(showUpdateSuccess){
        showUpdateSuccess.style.display = "block";
      }

      setTimeout(function(){
        if(showUpdateSuccess){
          showUpdateSuccess.style.display = "none"
        }
      }, 4000);
    });
  }

}
