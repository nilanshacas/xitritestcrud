import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PostApiService {

  readonly postAPIUrl = "https://localhost:7138/api";

  constructor(private http:HttpClient) { }

  getPostList():Observable<any[]>{
    return this.http.get<any>(this.postAPIUrl + '/XitriPosts');
  }

  addPost(data:any){
    return this.http.post(this.postAPIUrl + '/XitriPosts', data);
  }

  updatePost(id:number|string, data:any){
    return this.http.put(this.postAPIUrl + `/XitriPosts/${id}`, data);
  }

  deletePost(id:number|string){
    return this.http.delete(this.postAPIUrl + `/XitriPosts/${id}`);
  }
}
