import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { PostComponent } from './post/post.component';
import { ShowPostComponent } from './post/show-post/show-post.component';
import { AddEditPostComponent } from './post/add-edit-post/add-edit-post.component';

import { PostApiService } from './post-api.service'

@NgModule({
  declarations: [
    AppComponent,
    PostComponent,
    ShowPostComponent,
    AddEditPostComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [PostApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
