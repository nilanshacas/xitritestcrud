﻿using Microsoft.EntityFrameworkCore;
using XtTestAPI.Model;

namespace XtTestAPI.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }

        public DbSet<XitriPost> Posts { get; set; }
        public DbSet<User> Users { get; set; }
    }
}
