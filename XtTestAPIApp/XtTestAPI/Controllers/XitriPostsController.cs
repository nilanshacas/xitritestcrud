﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using XtTestAPI.Data;
using XtTestAPI.Model;

namespace XtTestAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class XitriPostsController : ControllerBase
    {
        private readonly DataContext _context;

        public XitriPostsController(DataContext context)
        {
            _context = context;
        }

        // GET: api/XitriPosts
        [HttpGet]
        public async Task<ActionResult<IEnumerable<XitriPost>>> GetPosts()
        {
          if (_context.Posts == null)
          {
              return NotFound();
          }
            return await _context.Posts.ToListAsync();
        }

        // GET: api/XitriPosts/5
        [HttpGet("{id}")]
        public async Task<ActionResult<XitriPost>> GetXitriPost(int id)
        {
          if (_context.Posts == null)
          {
              return NotFound();
          }
            var xitriPost = await _context.Posts.FindAsync(id);

            if (xitriPost == null)
            {
                return NotFound();
            }

            return xitriPost;
        }

        // PUT: api/XitriPosts/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutXitriPost(int id, XitriPost xitriPost)
        {
            if (id != xitriPost.Id)
            {
                return BadRequest();
            }

            _context.Entry(xitriPost).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!XitriPostExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/XitriPosts
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<XitriPost>> PostXitriPost(XitriPost xitriPost)
        {
          if (_context.Posts == null)
          {
              return Problem("Entity set 'DataContext.Posts'  is null.");
          }
            _context.Posts.Add(xitriPost);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetXitriPost", new { id = xitriPost.Id }, xitriPost);
        }

        // DELETE: api/XitriPosts/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteXitriPost(int id)
        {
            if (_context.Posts == null)
            {
                return NotFound();
            }
            var xitriPost = await _context.Posts.FindAsync(id);
            if (xitriPost == null)
            {
                return NotFound();
            }

            _context.Posts.Remove(xitriPost);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool XitriPostExists(int id)
        {
            return (_context.Posts?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
