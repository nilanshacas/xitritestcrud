﻿
namespace XtTestAPI.Model
{
    public class XitriPost
    {
        public int Id { get; set; }
        public string Title { get; set; } = string.Empty;
        public string Body { get; set; } = string.Empty;
        public string Status { get; set; } = string.Empty;
        public bool Active { get; set; }

    }
}
