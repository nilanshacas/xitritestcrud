import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { PostApiService } from 'src/app/post-api.service';


@Component({
  selector: 'app-show-post',
  templateUrl: './show-post.component.html',
  styleUrls: ['./show-post.component.css']
})
export class ShowPostComponent implements OnInit {

  postList$!:Observable<any[]>;

  constructor(private service:PostApiService) { }

  ngOnInit(): void {
    this.postList$ = this.service.getPostList();
  }

  modalTitle:string = '';
  activateAddEditPost:boolean = false;
  disableControls:boolean = false;
  post:any;

  // Delete a specific post
  deletePost(item:any){
    if(confirm(`Are you sure you want to delete post ${item.id}`)){
      this.service.deletePost(item.id).subscribe(res => {
        var closeModalBtn = document.getElementById('add-edit-modal-close')
        if(closeModalBtn){
          closeModalBtn.click()
        }

        var showDeleteSuccess = document.getElementById('delete-success-alert')
        if(showDeleteSuccess){
          showDeleteSuccess.style.display = "block";
        }

        setTimeout(function(){
          if(showDeleteSuccess){
            showDeleteSuccess.style.display = "none"
          }
        }, 4000);
        this.postList$ = this.service.getPostList();
      });
    }
  }



  // Models Activites
  addModal(){
    this.post = {
      id:0,
      title:null,
      body:null,
      status:null,
      active:false
    }
    this.modalTitle = "Add Post";
    this.activateAddEditPost = true;
    this.disableControls = false;
  }

  editModel(item:any){
    this.post = item;
    this.modalTitle = "Edit Post";
    this.activateAddEditPost = true;
    this.disableControls = false;
  }

  viewModel(item:any){
    this.post = item;
    this.modalTitle = "View Post";
    this.activateAddEditPost = true;
    this.disableControls = true;
  }

  closeModal(){
    this.activateAddEditPost = false;
    this.disableControls = false;
    this.postList$ = this.service.getPostList();
  }

}
